jQuery(document).ready(function(){
    jQuery('.wrap-left-tabs .tabs').on('click', 'li:not(.current)', function() {
        jQuery(this).addClass('current').siblings().removeClass('current').parents('.wrap-left-tabs').find('div.box').eq(jQuery(this).index()).fadeIn(150).siblings('div.box').hide();
    })
    jQuery('#search_mini_form label').on('click', function(){
        jQuery('#search_mini_form .input-box').fadeIn('fast');
    });
    jQuery('.in-input-box .close').on('click', function(){
        jQuery('#search_mini_form .input-box').fadeOut('fast');
    });
    jQuery('.category-slider-btn').on('click', function(){
         jQuery(this).parents('.block-category-slider').addClass('open');
    });
    jQuery('.block-category-slider .close').on('click', function(){
         jQuery(this).parents('.block-category-slider').removeClass('open');
         //initCarousel();
    });
    jQuery('.data-table tr:even').addClass('even');
    jQuery('.data-table tr:odd').addClass('odd');

    jQuery('.content-main-menu ul li .show-hide-btn').click(function(){
        jQuery(this).toggleClass('rotate').parent('li').toggleClass('open').find('ul').slideToggle();
    });

    jQuery('#mobile-nav .show-hide-btn').click(function(){
        jQuery(this).toggleClass('rotate').parent('li').toggleClass('open');
        jQuery(this).next('ul').slideToggle();
    });

    jQuery('.product-category-menu .title').click(function(){
        jQuery(this).next('ul').slideToggle().parent('div').toggleClass('open');
    });

    jQuery('.product-category-content .content li dd:last-child').addClass('last');
    jQuery('.product-category-content .content li dt:last-child').addClass('last');

    var popuptopmargin = (jQuery('#ajax-content-lightbox, #ajax-loadings, #ajax-recensie-lightbox, #ajax-success-lightbox').height() + 10) / 2;
    var popupleftmargin = (jQuery('#ajax-content-lightbox, #ajax-loadings, #ajax-recensie-lightbox, #ajax-success-lightbox').width() + 10) / 2;
    jQuery('#ajax-content-lightbox, #ajax-loadings, #ajax-recensie-lightbox, #ajax-success-lightbox').css({
        'margin-top' : -popuptopmargin,
        'margin-left' : -popupleftmargin
    });

    jQuery('#mob-nav-control').click(function(){
        jQuery(this).parents('body').find('.mobile-nav-content-overley').css('display', 'block');
        jQuery(this).parents('.wrapper').find('#mobile-nav').animate({'left': 0}, 400);
        jQuery(this).parents('.wrapper').find('.page').animate({'right': -233}, 400);
    });

    jQuery('.mobile-nav-content-overley').click(function(){
        jQuery(this).css('display', 'none');
        jQuery(this).next('.wrapper').find('#mobile-nav').animate({'left': -240}, 400);
        jQuery(this).next('.wrapper').find('.page').animate({'right': 0}, 400);
    });

    jQuery('.discount-btn').click(function(){
        jQuery(this).parent('.discount-code').find('.hide-item').show();
    });
    jQuery('.discount-code .close').click(function(){
        jQuery(this).parents('.discount-code').find('.hide-item').hide();
    });
});

function showFilters(el) {
    jQuery(el).next('ul').slideToggle();
}
function offFiltersEvent(){
    jQuery('.product-category-content .content-top .filter li').off('click', function(){
        jQuery(this).parent('ul').hide('fast');
    });
}
function initCloseFilters(){
    jQuery('.product-category-content .content-top .filter li').on('click', function(){
        jQuery(this).parent('ul').hide('fast');
    });
}

jQuery('#nav .level0').each(function(){
    var listItem = jQuery(this);
    listItem.find('.nav-drop').addClass('nav-drop-parent');
});

jQuery( document ).ready(function() {
    jQuery('#nav .level0').hover(
        function(){
            jQuery(this).addClass('hover').find('.nav-drop').show();
        },
        function(){
            jQuery(this).removeClass('hover').find('.nav-drop').hide();
        });
    jQuery('.nav-drop-parent').parents('#nav .level0').addClass('drop');
});
